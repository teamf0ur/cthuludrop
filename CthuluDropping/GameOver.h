//
//  GameOver.h
//  cthuludropping
//
//  Created by Andrew Hawkins, David Hope, Jose Paolo Gonzales Otico, and Greg Roche on 2014-11-30.
//

#import <SpriteKit/SpriteKit.h>

@interface GameOver : SKScene

-(id)initWithSize:(CGSize)size won:(BOOL)won score: (NSInteger)player_score;

@end
