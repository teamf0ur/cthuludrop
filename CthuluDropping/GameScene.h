//
//  GameScene.h
//  CthuluDropping
//
//  Created by Andrew Hawkins, David Hope, Jose Paolo Gonzales Otico, and Greg Roche on 2014-11-30.
//

#import <SpriteKit/SpriteKit.h>
#import "PBParallaxScrolling.h"

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@property (nonatomic) PBParallaxBackgroundDirection direction;
@property int player_score; // player score property

-(id)initWithSize:(CGSize)size andDirection: (PBParallaxBackgroundDirection) direction;

@end
