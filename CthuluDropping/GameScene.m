//
//  GameScene.m
//  CthuluDropping
//
//  Created by Andrew Hawkins, David Hope, Jose Paolo Gonzales Otico, and Greg Roche on 2014-11-30.
//

#import "GameScene.h"
#import "FMMParallaxNode.h"
#import "GameOver.h"

#pragma mark - Custom Type Definitions
#define kScoreHudName @"scoreHud"
#define kHealthHudName @"healthHud"

@interface GameScene ()
@property BOOL contentCreated;
@property (nonatomic) NSTimeInterval lastSpawnTimeInterval;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;
@property BOOL boosting;
@property (nonatomic, retain) SKLabelNode* scoreLabel;  // score label on the hud
@property (nonatomic, retain) SKLabelNode* speedLabel;  // speed label on the hud
@property (nonatomic, strong) PBParallaxScrolling * parallaxBackground;
@end

@implementation GameScene

{
    // Set the global variables needed for the application
    SKSpriteNode *_cthulhu;
    SKSpriteNode *item;
    SKSpriteNode *cloud;
    SKSpriteNode *_flames;
    SKNode *_wall;
    NSTimeInterval _lastUpdateTime;
    NSTimeInterval _deltaTime;
    NSMutableArray *_items;
    NSMutableArray *_clouds;
    NSMutableArray *_explosionTextures;
    PBParallaxScrolling * parallax;
#define kNumItems   1
#define kNumCloud   1
    int _groundDistance;
    bool _groundCreated;
    bool _portalCreated;
    int _speed;
    int _nextItem;
    int _acceleration;
    int _accelCounter;
    int _portalTimer;
    int _groundTimer;
    bool _spawnsActive;
    double _nextItemSpawn;
    double _nextCloudSpawn;
}

-(id)initWithSize:(CGSize)size andDirection: (PBParallaxBackgroundDirection) direction {
    if (self = [super initWithSize:size]) {
        // Set important globals to default values
        _spawnsActive = YES;
        _groundCreated = NO;
        _portalCreated = NO;
        _acceleration = 1;
        _accelCounter = 0;
        _portalTimer = 0;
        _groundTimer = 0;
        // Generate a random number between 500 and 20,000. This will be the range of how far the game over wall will appear
        _groundDistance = [self randomIntegerBetween:500 and:20000];
        // Set speed of how fast Cthulhu is falling. This will be used to subtract from groundDistance each second
        _speed = 20;
        
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.physicsWorld.contactDelegate = self;
        _boosting = NO;
        [self setupHud];
        
        // Create the Portal Node and add to the scene
        [self addChild:[self portalNode]];
    }
    
#pragma mark - Game Backgrounds
    self.direction = direction;
    NSArray * imageNames;
    imageNames= @[ @"pBackgroundVertical"];
    parallax = [[PBParallaxScrolling alloc] initWithBackgrounds:imageNames size:size direction:direction fastestSpeed:kPBParallaxBackgroundDefaultSpeed andSpeedDecrease:kPBParallaxBackgroundDefaultSpeedDifferential];
    self.parallaxBackground = parallax;
    [self addChild:parallax];
    
#pragma mark - Setup Sprite for Cthulhu
    _cthulhu = [SKSpriteNode spriteNodeWithImageNamed:@"Cthulhu_image_sm.png"];
    [_cthulhu setScale:1.5];
    _cthulhu.name = @"csprite";
    _cthulhu.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_cthulhu.frame.size];
    _cthulhu.physicsBody.dynamic = YES;
    _cthulhu.physicsBody.categoryBitMask = 1;
    _cthulhu.physicsBody.contactTestBitMask = 2;
    _cthulhu.physicsBody.collisionBitMask =  0;
    _cthulhu.position = CGPointMake(160,500);
    _cthulhu.physicsBody.usesPreciseCollisionDetection = YES;
    SKAction *rotation = [SKAction rotateByAngle: M_PI duration:0.5];
    [_cthulhu runAction: rotation];
    [self addChild:_cthulhu];
#pragma mark - TBD - Setup the items
    
    _items = [[NSMutableArray alloc] initWithCapacity:kNumItems];
    
    for (int i = 0; i < kNumItems; ++i) {
        item = [self randomItem];
        [_items addObject:item];
        [self addChild:item];
    }
    
    _clouds = [[NSMutableArray alloc] initWithCapacity:kNumCloud];
    
    for (int i = 0; i < kNumCloud; i++) {
        cloud = [self randomCloud];
        [_clouds addObject:cloud];
        [self addChild:cloud];
    }
    
    return self;
}




-(void)setupHud { // Heads Up Display for Score & Speed
    // root node with black background
    SKSpriteNode* hud = [[SKSpriteNode alloc] initWithColor:[UIColor blackColor] size:CGSizeMake(self.size.width, self.size.height*0.05)];
    hud.anchorPoint=CGPointMake(0, 1);
    hud.position = CGPointMake(0,self.size.height-hud.size.height);
    hud.zPosition = 1001;
    [self addChild:hud];
    
    // player score on the hud
    self.player_score = 0;
    self.scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    self.scoreLabel.name = kScoreHudName;
    self.scoreLabel.fontSize = 30;
    self.scoreLabel.fontColor = [SKColor greenColor];
    self.scoreLabel.text = [NSString stringWithFormat:@"DPV: %04d", _player_score];
    self.scoreLabel.position = CGPointMake(20 + _scoreLabel.frame.size.width/2, self.size.height - (70 + _scoreLabel.frame.size.height/2));
    self.scoreLabel.zPosition = 1002;
    [self addChild:_scoreLabel];
    
    // speed of cthuhlu on the hud
    self.speedLabel = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    self.speedLabel.name = kHealthHudName;
    self.speedLabel.fontSize = 30;
    self.speedLabel.fontColor = [SKColor redColor];
    self.speedLabel.text = [NSString stringWithFormat:@"Speed: %d ft/s", _speed];
    self.speedLabel.position = CGPointMake(self.size.width - _speedLabel.frame.size.width/2 - 20, self.size.height - (70 + _speedLabel.frame.size.height/2));
    self.speedLabel.zPosition = 1003;
    [self addChild:_speedLabel];
}

// Creates the Portal Node to display on the scene
- (SKSpriteNode *) portalNode {
    SKSpriteNode *portal = [SKSpriteNode spriteNodeWithImageNamed:@"PortalButton"];
    portal.name = @"portal";
    [portal setScale:0.5];
    portal.position = CGPointMake(self.size.width/2, 830);
    portal.zPosition = 1000;
    return portal;
}


/*
 Generates a random number, and defines a sprite based on the number,
 creates the physics body for collision, and returns the item.
 */
-(SKSpriteNode *) randomItem {
    int randNumber = [self randomIntegerBetween:1 and: 3];
    
    switch(randNumber)
    {
        case 1:
            item = [SKSpriteNode spriteNodeWithImageNamed:@"soul.png"];
            item.name = @"soul";
            break;
        case 2:
            item = [SKSpriteNode spriteNodeWithImageNamed:@"bieber.png"];
            item.name = @"bieber";
            break;
        case 3:
            item = [SKSpriteNode spriteNodeWithImageNamed:@"boost.png"];
            item.name = @"boost";
            break;
    }
    
    item.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:item.frame.size];
    item.physicsBody.dynamic = true;
    item.physicsBody.categoryBitMask = 2;
    item.physicsBody.contactTestBitMask = 1;
    item.physicsBody.collisionBitMask =  0;
    item.physicsBody.usesPreciseCollisionDetection = YES;
    item.hidden = YES;
    return item;
}

// The same as randomItem, expect it generates one of three types of clouds
-(SKSpriteNode *) randomCloud {
    
    int randNumber = [self randomIntegerBetween:1 and: 3];
    
    switch(randNumber)
    {
        case 1:
            cloud = [SKSpriteNode spriteNodeWithImageNamed:@"Clouds_Dark.png"];
            cloud.name = @"darky";
            cloud.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:cloud.frame.size];
            cloud.physicsBody.dynamic = true;
            cloud.physicsBody.categoryBitMask = 2;
            cloud.physicsBody.contactTestBitMask = 1;
            cloud.physicsBody.collisionBitMask =  0;
            cloud.physicsBody.usesPreciseCollisionDetection = YES;
            cloud.hidden = YES;
            break;
        case 2:
            cloud = [SKSpriteNode spriteNodeWithImageNamed:@"Clouds_Plain.png"];
            cloud.name = @"plain";
            cloud.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:cloud.frame.size];
            cloud.physicsBody.dynamic = true;
            cloud.physicsBody.categoryBitMask = 4;
            cloud.physicsBody.contactTestBitMask = 4;
            cloud.physicsBody.collisionBitMask =  0;
            cloud.physicsBody.usesPreciseCollisionDetection = YES;
            break;
        case 3:
            cloud = [SKSpriteNode spriteNodeWithImageNamed:@"Clouds_Smile.png"];
            cloud.name = @"smile";
            cloud.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:cloud.frame.size];
            cloud.physicsBody.dynamic = true;
            cloud.physicsBody.categoryBitMask = 2;
            cloud.physicsBody.contactTestBitMask = 1;
            cloud.physicsBody.collisionBitMask =  0;
            cloud.physicsBody.usesPreciseCollisionDetection = YES;
            break;
    }
    
    cloud.hidden = YES;
    cloud.zPosition = [self randomIntegerBetween:30 and:50];
    return cloud;
    
}



// Handle the Touch event of the Portal Node to safety
-(void) touchesBegan:(NSSet *) touches withEvent:(UIEvent *)event
{
    NSLog(@"got here");
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    SKNode *csprite = [self childNodeWithName:@"csprite"];
    CGPoint positionInScene = [touch locationInNode:self];
    
    int actualDuration = [self calcDuration:2 andMin:1];
    if ([node.name isEqualToString:@"portal"] && _boosting == NO && _portalCreated == NO)
    {
        [self createGroundOrPortal:node.name];
        _portalCreated = YES;
    }
    else
    {
    // Create the actions
    SKAction * actionMove = [SKAction moveTo:CGPointMake(positionInScene.x, csprite.position.y) duration:actualDuration];
    [csprite runAction:actionMove];
        if(_boosting == YES)
        {
            SKNode *boost = [self childNodeWithName:@"flames"];
            SKAction * boostMove = [SKAction moveTo:CGPointMake(positionInScene.x,boost.position.y) duration:actualDuration];
            [boost runAction:boostMove];
            
        }
    }
    

 
}

// Updates every second to check and update various variables
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    
    self.lastSpawnTimeInterval += timeSinceLast;
    if (self.lastSpawnTimeInterval > 1) {
        self.lastSpawnTimeInterval = 0;
        NSLog(@"%d", _groundDistance);
        // Cthulhu can naturally far faster as time goes on, and accelerates when falling for long periods of time. There is no limit to how fast Cthulhu falls.
        if (_boosting != YES) {
            _groundDistance -= _speed;
            _speed += _acceleration;
            self.speedLabel.text = [NSString stringWithFormat:@"Speed: %d ft/s", _speed];
            _accelCounter++;
            // If 5 seconds pass while Cthulhu is falling and the ground hasn't spawned, increase the acceleration
            if (_accelCounter > 5 && _groundCreated == NO) {
                _acceleration++;
                _accelCounter = 0;
            }
        }
        else {
            // Boost is active, sending Cthulhu up instead of down
            _speed = 100;
            _groundDistance += _speed;
            self.speedLabel.text = [NSString stringWithFormat:@"Speed: %d ft/s", _speed];
        }
        // Set the active spawner of items and clouds to off when below a threshold.
        if (_groundDistance <= 1500) {
            _spawnsActive = NO;
        }
        // Check if the ground distance has gone below zero. If it does create the ground and set the spawn rate of items to zero
        if (_groundDistance <= 0) {
            if (_groundCreated == NO) {
                _groundCreated = YES;
                _spawnsActive = NO;
                [self createGroundOrPortal:(@"ground")];
            // This code is in the unlikely event that the ground is created, and then swiftly removed
            } else {
                _groundTimer++;
                if (_groundTimer > 3) {
                    _groundCreated = NO;
                    _groundTimer = 0;
                }
            }
            
        }
        
        // Check if the user created a Portal. If they have increment the time it has been since one was created by 1
        if (_portalCreated == YES) {
            _portalTimer++;
            // If the timer exceeds 10 seconds the user can create a portal again
            if (_portalTimer > 10) {
                _portalCreated = NO;
                _portalTimer = 0;
            }
        }
        
    }
}

// Generate the game ending wall and have it act like a normal item, only with it's animation speed increasing along with the speed Cthulhu is falling
- (void)createGroundOrPortal:(NSString *) type {
    // Create Wall and assign it to the _items array
    SKSpriteNode * endGameItem;
    if ([type isEqual:(@"ground")]) {
        endGameItem = [SKSpriteNode spriteNodeWithImageNamed:(@"Ground")];
        endGameItem.name = type;
        //Set it's location. It will spawn outside the area and move up towards Cthulhu
        endGameItem.position = CGPointMake(endGameItem.size.width - 400, -self.frame.size.height + [self randomIntegerBetween:100 and:500]);
        [self addChild:(endGameItem)];
    }
    else if ([type isEqual:(@"portal")]) {
        endGameItem = [SKSpriteNode spriteNodeWithImageNamed:(@"Portal")];
        endGameItem.name = type;
        //Set it's location. It will spawn outside the area and move up towards Cthulhu
        endGameItem.position = CGPointMake(endGameItem.size.width/2 + 75, -self.frame.size.height + [self randomIntegerBetween:300 and:500]);
        [self addChild:(endGameItem)];
    }
    endGameItem.zPosition = 1010;
    [_items addObject:endGameItem];
    
    
    //Create it's physics and categorize as an item.
    endGameItem.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:endGameItem.size];
    endGameItem.physicsBody.dynamic = YES;
    endGameItem.physicsBody.categoryBitMask = 2;
    endGameItem.physicsBody.contactTestBitMask = 1;
    endGameItem.physicsBody.collisionBitMask = 0;
    
    //Determine the item's speed. This will be determined by speed in increments of 20, 20 being the slowest and 200 being the fastest, and whether it's the ground or the portal being made
    int duration;
    if (_speed >= 20 && _speed < 40) {
        duration = 5;
    }
    else if (_speed >= 40 && _speed < 60){
        duration = 4.5;
    }
    else if (_speed >= 60 && _speed < 80) {
        duration = 4;
    }
    else if (_speed >= 80 && _speed < 100) {
        duration = 3.5;
    }
    else if (_speed >= 100 && _speed < 120){
        duration = 3;
    }
    else if (_speed >= 120 && _speed < 140){
        duration = 2.5;
    }
    else if (_speed >= 140 && _speed < 160){
        duration = 2;
    }
    else if (_speed >= 160 && _speed < 180){
        duration = 1.5;
    }
    else {
        duration = 1;
    }
    //Create the item's actions
    if ([type isEqual:@"ground"]) {
        SKAction * action = [SKAction moveTo:CGPointMake(endGameItem.size.width - 400, 176) duration: duration];
        [endGameItem runAction:[SKAction sequence:@[action]]];
    }
    else {
        SKAction * action = [SKAction moveTo:CGPointMake(endGameItem.size.width/2 + 75, self.frame.size.height + 350) duration: duration];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [endGameItem runAction:[SKAction sequence:@[action, actionMoveDone]]];
    }
}

/*
 Called when two physics bodies make contact - typically the player and an item,
 loops through the items and looks for the one that intersected cthulhu, and removes
 that item from view. Power up actions will be defined here.
 */
-(void)didBeginContact:(SKPhysicsContact *)contact {
    SKPhysicsBody *firstBody, *secondBody;
    
    firstBody = contact.bodyA;
    secondBody = contact.bodyB;
    
    if(firstBody.categoryBitMask == 2 || secondBody.categoryBitMask == 2)
    {
        for (SKSpriteNode *itemd in _items) {
            if (itemd.hidden) {
                continue;
            }
            
            if ([_cthulhu intersectsNode:itemd ]) {
                if(![itemd.name  isEqual:@"ground"])
                {
                    itemd.hidden = YES;
                }
                SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
                                                       [SKAction fadeInWithDuration:0.1]]];
                SKAction *blinkSlow = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.5],
                                                           [SKAction fadeInWithDuration:0.5]]];
                SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
                SKAction *blinkExtended = [SKAction repeatAction:blinkSlow count:25];
                [_cthulhu runAction:blinkForTime];
                if ([itemd.name isEqual:@"bieber"]) {
                    // Bieber gives more point than a normal soul, but it also causes Cthulhu to fall faster
                    self.player_score += 500;
                    [self runAction:[SKAction playSoundFileNamed:@"nom 2.wav" waitForCompletion:NO]];
                    _acceleration++;
                    _accelCounter = 0;
                }
                else if ([itemd.name isEqual:@"soul"]) {
                    // A standard soul gives Cthulhu some points, but it's pretty bland and doesn't do much else
                    self.player_score += 250;
                    [self runAction:[SKAction playSoundFileNamed:@"nom 1.wav" waitForCompletion:NO]];
                }
                else if([itemd.name  isEqual: @"boost"])
                {
                    // A boost gives the least amount of points but causes Cthulhu to go upwards!
                    self.player_score += 100;
                    // While boosting items and clouds should not appear, and any onscreen should disappear
                    _spawnsActive = NO;
                    for (SKSpriteNode *itemd in _items) {
                        [itemd removeFromParent];
                    }
                    for (SKSpriteNode *cloudd in _clouds) {
                        [cloudd removeFromParent];
                        
                    }
                    // Make sure Cthulhu isn't already boosting, just in case
                    if (_boosting != YES) {
                        // Rotate Cthulhu 180 degrees as he is now going up
                        SKAction *rotation = [SKAction rotateByAngle: M_PI duration:0.5];
                         [_cthulhu removeAllActions];
                        [_cthulhu runAction: rotation];
                        // Create the Fire showing the powerup is active
                        _flames = [SKSpriteNode spriteNodeWithImageNamed:@"flamesreverse.png"];
                        _flames.name = @"flames";
                        _boosting = YES;
                        _flames.position = CGPointMake(_cthulhu.position.x,_cthulhu.position.y-95);
                        [self addChild:_flames];
                        
                        [_flames runAction:blinkExtended];
                       
                        // Play the rocket sound effect
                       [self runAction:[SKAction playSoundFileNamed:@"rocket.wav" waitForCompletion:NO]];
                        // Have the background reverse direction to show upward movement
                        [self.parallaxBackground reverseMovementDirection];
                        // Run the method to track how long the powerup has lasted and remove it once time expires
                        [self powerUpTimeOut:@"boosting"];
                    }
                }
                else if ([itemd.name isEqual:@"ground"]) {
                    // The ground is the Game Over object that will cause the player to lose, or rather loses lots of points. Hitting the ground cause Cthulhu to explode. Just cause.
                    NSMutableArray *explodecompilation = [self generateExplosion];
                    
                    SKSpriteNode *explosion = [self createExplosionNode:(SKSpriteNode *)contact.bodyA.node :explodecompilation];
                    
                    [self addChild:explosion];
                    [_cthulhu removeFromParent];
                    [parallax stopParallax];
                    
                    // Ensure any onscreen items or clouds explode upon impact
                    _spawnsActive = NO;
                    for (SKSpriteNode *itemd in _items) {
                        if (![itemd.name isEqual: @"ground"]) {
                            NSMutableArray *explodecompilation = [self generateExplosion];
                            SKSpriteNode *explosion = [self createExplosionNode:itemd :explodecompilation];
                            [self addChild:explosion];
                            [itemd removeFromParent];
                            [self explosionAnimation:explosion :explodecompilation];
                        }
                    }
                    for (SKSpriteNode *cloudd in _clouds) {
                        NSMutableArray *explodecompilation = [self generateExplosion];
                        SKSpriteNode *explosion = [self createExplosionNode:cloudd :explodecompilation];
                        [self addChild:explosion];
                        [cloudd removeFromParent];
                        [self explosionAnimation:explosion :explodecompilation];
                    }
                    // Play the explosion sound and animation
                    [self runAction:[SKAction playSoundFileNamed:@"Boom.wav" waitForCompletion:NO]];
                    [self explosionAnimation:explosion :explodecompilation];
                    
                    
                    // Deduct the player's score for failing Cthulhu. A flat 10000 is removed and the final score is mutilpled by 1/10 the speed Cthulhu was at when he impacted the ground. Cthulhu's base falling speed is 20 ft so bare minimum the mutliplier will be x2
                    self.player_score = (self.player_score - 10000) * (_speed / 10);
                    _speed = 0;
                    _acceleration = 0;
                    id wait = [SKAction waitForDuration:1.5];
                    id run = [SKAction runBlock:^{
                        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
                        SKScene * gameOver = [[GameOver alloc] initWithSize:self.size won:NO score:self.player_score];
                        [self.view presentScene:gameOver transition: reveal];
                    }];
                    [self runAction:[SKAction sequence:@[wait, run]]];
                    
                    
                    
                }
                else if ([itemd.name isEqual:@"portal"]) {
                    // Cthulhu is alive! To determine this score, give a base 2000 and deduct how far Cthulhu was from the ground when entering the portal, then multiply by 1/20 the speed Cthulhu was going. Cthulhu's base falling speed is 20 ft so bare minimum the mutliplier will be x1
                    self.player_score = (self.player_score + 2000 - _groundDistance) * (_speed / 20);
                    _speed = 0;
                    _acceleration = 0;
                    SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
                    SKScene * gameOver = [[GameOver alloc] initWithSize:self.size won:YES score:self.player_score];
                    [self.view presentScene:gameOver transition: reveal];
                }
            }
        }
        // Clouds are unique as they don't show any signs of doing anything, but some do
        for (SKSpriteNode *cloudd in _clouds) {
            if (cloudd.hidden) {
                continue;
            }
            if ([_cthulhu intersectsNode:cloudd]) {
                // A smiley cloud is full of hideous joy and cheer, and causes Cthulhu's score to go down
                if ([cloudd.name isEqual:@"smile"]) {
                    self.player_score -= 1500;
                }
                // A dark cloud regenergizes Cthulhu's dark spirit
                else if ([cloudd.name isEqual:@"darky"]) {
                    self.player_score += 600;
                }
            }
        }
        
        // update score on the hud
        self.scoreLabel.text=[NSString stringWithFormat:@"DPV: %04d", _player_score];
    }}

// Method to grab the Explosion images from the atlas folder and save them to an array
-(NSMutableArray *) generateExplosion {
    SKTextureAtlas *explosionAtlas = [SKTextureAtlas atlasNamed:@"ExplosionImages"];
    NSArray *textureNames =[explosionAtlas textureNames];
    _explosionTextures = [NSMutableArray new];
    for (NSString *name in textureNames) {
        SKTexture *texture = [explosionAtlas textureNamed:name];
        [_explosionTextures addObject:texture];
    }
    return _explosionTextures;
}

// Method to create the actual Explosion node to replace the object that is doing the sploding
-(SKSpriteNode *) createExplosionNode :(SKSpriteNode *) spriteNode :(NSMutableArray *) explodecompilation{
    SKSpriteNode *explosion = [SKSpriteNode spriteNodeWithTexture:[explodecompilation objectAtIndex:0]];
    explosion.zPosition = 1;
    explosion.scale = 2;
    explosion.position = spriteNode.position;
    return explosion;
}

// Build and run the explosion animation for items, clouds and Cthulhu exploding
-(void)explosionAnimation :(SKSpriteNode*) explosion :(NSMutableArray*) explodecompilation{
    SKAction *explosionAction = [SKAction animateWithTextures:explodecompilation timePerFrame:0.07];
    SKAction *remove = [SKAction removeFromParent];
    [explosion runAction:[SKAction sequence:@[explosionAction, remove]]];
}

//Generates a random integer between two numbers
- (NSInteger)randomIntegerBetween:(NSInteger)min and:(NSInteger)max {
    return (NSInteger)(min + arc4random_uniform(max - min + 1));
}

//Generates a random float between two numbers
- (float)randomValueBetween:(float)low andValue:(float)high {
    return (((float) arc4random() / 0xFFFFFFFFu) * (high - low)) + low;
}

// Calculate a random number to be used for Duration of node move actions
- (int) calcDuration:(int)maxDuration andMin:(int)minDuration {
    int duration;
    int rangeduration = maxDuration - minDuration;
    duration = (arc4random() % rangeduration) + minDuration;
    return duration;
}
//After specified time has passed, remove boost power up.
-(void)powerUpTimeOut:(NSString *)powerup {
    id wait = [SKAction waitForDuration:3.3];
    id run = [SKAction runBlock:^{
        if([powerup  isEqual: @"boosting"])
        {
            [_flames removeFromParent];
            // Rotate Cthulhu back to his start position
            SKAction *rotation = [SKAction rotateByAngle: M_PI duration:0.5];
            [_cthulhu runAction: rotation];
            // Set variables changes by the boost back to normal and reverse the direction of the background
            _boosting = NO;
            _spawnsActive = YES;
            _speed = 20;
            _acceleration = 1;
            _accelCounter = 0;
            [self.parallaxBackground reverseMovementDirection];
        }
    }];
    [_flames runAction:[SKAction sequence:@[wait, run]]];
}

-(void)update:(NSTimeInterval)currentTime {
    int actualDuration = [self calcDuration:10 andMin:5];
    [self.parallaxBackground update:currentTime];
    // Handle time delta in cause the fps goes below 60
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) {
        timeSinceLast = 1.0 / 60.0;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    [self updateWithTimeSinceLastUpdate:timeSinceLast];
    
    // Timer to determine when to create Items
    double curTime = CACurrentMediaTime();
    if (curTime > _nextItemSpawn) {
        
        float randSecs = [self randomValueBetween:0.20 andValue:20.0];
        _nextItemSpawn = randSecs + curTime;
        
        float randX = [self randomValueBetween:0.0 andValue:self.frame.size.width];
        if(_spawnsActive == YES)
        {
            item = [self randomItem];
            [_items addObject:item];
            [self addChild:item];
        }
        
        
        [item removeAllActions];
        item.position = CGPointMake(randX, 0);
        item.hidden = NO;
        
        CGPoint location = CGPointMake(item.position.x, self.frame.size.height + 250);
        
        SKAction *moveAction = [SKAction moveTo:location duration:actualDuration];
        SKAction *doneAction = [SKAction removeFromParent];
        [item runAction:[SKAction sequence:@[moveAction, doneAction]]];
        
    }
    // Timer to determine when to create Clouds
    if(curTime > _nextCloudSpawn)
    {
        float randSecs = [self randomValueBetween:0.20 andValue:20.0];
        _nextCloudSpawn = randSecs + curTime;
        
        float randX = [self randomValueBetween:0.0 andValue:self.frame.size.width];
        if(_spawnsActive == YES)
        {
            cloud = [self randomCloud];
            [_clouds addObject:cloud];
            [self addChild:cloud];
        }
        
        
        [cloud removeAllActions];
        cloud.position = CGPointMake(randX, 0);
        cloud.hidden = NO;
        
        CGPoint location = CGPointMake(cloud.position.x, self.frame.size.height + 250);
        
        SKAction *moveAction = [SKAction moveTo:location duration:actualDuration];
        SKAction *doneAction = [SKAction removeFromParent];
        [cloud runAction:[SKAction sequence:@[moveAction, doneAction]]];
        
    }
}


@end