//
//  ViewController.m
//  CthuluDropping
//
//  Created by Andrew Hawkins, David Hope, Jose Paolo Gonzales Otico, and Greg Roche on 2014-11-30.
//

#import "ViewController.h"
#import <SpriteKit/SpriteKit.h>
#import "GameMenu.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController()

@property (nonatomic) AVAudioPlayer * backgroundMusicPlayer;    // Background music property

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * gameView = (SKView *)self.view;
    gameView.showsFPS = NO;
    gameView.showsNodeCount = NO;
    
    // Play endless madness music
    NSError *error;
     NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"CthuhluSong" withExtension:@"mp3"];
     self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
     self.backgroundMusicPlayer.numberOfLoops = -1;
     [self.backgroundMusicPlayer setVolume:0.1];
     [self.backgroundMusicPlayer prepareToPlay];
     [self.backgroundMusicPlayer play];
}

- (void)viewWillAppear:(BOOL)animated
{
    GameMenu *menu = [[GameMenu alloc] initWithSize: CGSizeMake(640, 960)];
    SKView *gameView = (SKView *) self.view;
    [gameView presentScene:menu];
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
