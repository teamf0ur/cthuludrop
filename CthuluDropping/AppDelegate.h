//
//  AppDelegate.h
//  CthuluDropping
//
//  Created by Andrew Hawkins on 2014-11-30.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
