//
//  MyScene.m
//  CthuluDropping
//
//  Created by Andrew Hawkins, David Hope, Jose Paolo Gonzales Otico, and Greg Roche on 2014-11-30.
//

#import "GameMenu.h"
#import "GameScene.h"
#import <AVFoundation/AVFoundation.h>

@interface GameMenu ()

@property BOOL contentCreated;

@end

@implementation GameMenu
SKSpriteNode *Back;
NSArray *planeBackground;


-(void) didMoveToView:(SKView *)view
{
    if (!self.contentCreated)
    {
        [self createSceneContents];
        self.contentCreated = YES;
    }
    
}
-(void)movingBackground
{
    // This causes the Animated Background to repeat while on the Menu
    [Back runAction:[SKAction repeatActionForever:
                      [SKAction animateWithTextures:planeBackground
                                       timePerFrame:0.18f
                                             resize:NO
                                            restore:YES]] withKey:@"compiledBackground"];
    return;
}

-(void) createSceneContents
{
    // Pull in the images from the atlas folder CthulhuImages and add them to an array
    NSMutableArray *imageFrames = [NSMutableArray array];
    SKTextureAtlas *imageAnimatedAtlas = [SKTextureAtlas atlasNamed:@"CthulhuImages"];
    int numImages = imageAnimatedAtlas.textureNames.count;
    for (int i=1; i <= numImages; i++) {
        NSString *textureName = [NSString stringWithFormat:@"Cthulu%d", i];
        SKTexture *temp = [imageAnimatedAtlas textureNamed:textureName];
        [imageFrames addObject:temp];
    }
    // Set the background to the array of images
    planeBackground = imageFrames;
    SKTexture *temp = planeBackground[0];
    // Create the Label for the Game Title
    SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Futura-CondensedExtraBold"];
    title.text = @"Cthulu Drop";
    title.fontSize = 62;
    title.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + 230);
    Back = [SKSpriteNode spriteNodeWithTexture:temp];
    Back.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    self.scaleMode = SKSceneScaleModeAspectFit;
    // Add the background and Game Title to the screen
    [self addChild: Back];
    [self movingBackground];
    [self addChild: title];
    // Add the User Option labels using the defined method newMenuNode
    [self addChild:[self newMenuNode:@"Exit Game" withName:@"Exit Node" withY:-55]];
    [self addChild:[self newMenuNode:@"Start Game" withName:@"Start Node" withY:55]];
    
}

// Creates a Label Node with the parameters to support touchesBegan method configured
-(SKLabelNode *) newMenuNode:(NSString*) text withName:(NSString*) name withY: (int) Y
{
    SKLabelNode *menuNode = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
    
    menuNode.text = text;
    menuNode.name = name;
    menuNode.fontSize = 40;
    menuNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + Y);
    
    return menuNode;
}

// Method to handle Touch Events
-(void) touchesBegan:(NSSet *) touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    // Begin the game if user selects Start Game, otherwise exit out
    if ([node.name isEqualToString:@"Start Node"])
    {
            SKScene *GamingScene = [[GameScene alloc] initWithSize:self.size andDirection:kPBParallaxBackgroundDirectionUp];
            SKTransition *doors = [SKTransition doorsOpenVerticalWithDuration:1.5];
            [self.view presentScene:GamingScene transition:doors];
    }
    else if ([node.name isEqualToString:@"Exit Node"])
    {
        exit(0);
    }
}

@end
