//
//  GameOver.m
//  cthuludropping
//
//  Created by Andrew Hawkins, David Hope, Jose Paolo Gonzales Otico, and Greg Roche on 2014-11-30.
//

#import "GameOver.h"
#import "GameScene.h"
#import "GameMenu.h"

@implementation GameOver

-(id)initWithSize:(CGSize)size won:(BOOL)won score:(NSInteger)player_score {
    if (self = [super initWithSize:size]) {
        
        // Determine if the user survived or hit the ground and display the message and image accordingly
        NSString * message;
        if (won) {
            message = @"You Have Survived!";
            
        } else {
            message = @"You Go Splat!";
        }
        
        // Display the proper image depending on the player's score
        if (player_score <= 0) {
            SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"disappointed_cthulhu.png"];
            bgImage.position = CGPointMake(self.size.width/2,self.size.height/2);
            bgImage.scale = 1.5;
            [self addChild:bgImage];
        }
        else {
            SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"winning_Cthulhu.png"];
            bgImage.position = CGPointMake(self.size.width/2,self.size.height/2);
            bgImage.scale = 1.5;
            [self addChild:bgImage];
        }
        
        // Create the Label to display the Game Over message
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Futura-CondensedExtraBold"];
        label.text = message;
        label.fontSize = 40;
        label.fontColor = [SKColor yellowColor];
        label.position = CGPointMake(self.size.width/2, self.size.height/2 + 100);
        [self addChild:label];
        
        // Create the label to display the player's score
        NSString * playerscoremsg = [NSString stringWithFormat:@"Dark Power Value: %ld",(long)player_score];
        
        SKLabelNode *playerscore = [SKLabelNode labelNodeWithFontNamed:@"Futura-CondensedExtraBold"];
        playerscore.text = playerscoremsg;
        playerscore.fontSize = 38;
        playerscore.fontColor = [SKColor yellowColor];
        playerscore.position = CGPointMake(self.size.width/2, 360);
        playerscore.name = @"DPV";
        [self addChild:playerscore];
        
        // Create the Labels to play the game again or exit
        SKLabelNode *goAgain = [SKLabelNode labelNodeWithFontNamed:@"Futura-CondensedExtraBold"];
        goAgain.text = @"Play Again";
        goAgain.name = @"again";
        goAgain.fontSize = 30;
        goAgain.fontColor = [SKColor yellowColor];
        goAgain.position = CGPointMake(self.size.width/2, self.size.height/2 - 220);
        [self addChild:goAgain];
        
        SKLabelNode *exitGame = [SKLabelNode labelNodeWithFontNamed:@"Futura-CondensedExtraBold"];
        exitGame.text = @"Exit Game";
        exitGame.name = @"exit";
        exitGame.fontSize = 30;
        exitGame.fontColor = [SKColor yellowColor];
        exitGame.position = CGPointMake(self.size.width/2, self.size.height/2 - 300);
        [self addChild:exitGame];
        
    }
    return self;
}

// Handles the case of which label the user tapped: Play Again or Exit Game
-(void) touchesBegan:(NSSet *) touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"again"])
    {
        [self runAction:[SKAction sequence:@[[SKAction runBlock:^{
            // 5
            SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
            SKScene * gameMenu = [[GameScene alloc] initWithSize:self.size andDirection:kPBParallaxBackgroundDirectionUp];
            [self.view presentScene:gameMenu transition: reveal];
        }]]]];
    }
    else if ([node.name isEqualToString:@"exit"]){
        exit(0);
    }
}

@end
